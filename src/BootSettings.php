<?php // ; -*- mode: php; eval: (drupal-mode); -*-

namespace Drupal\boot;

/**
 * Boot theme settings helper.
 */
class BootSettings {

  /** @var  $ */
  const BOOT_SETTINGS_KEY = "boot.settings";

  /**
   *
   */
  public static function get(string ...$keys) {
    $config = \Drupal::service("config.typed")->get(self::BOOT_SETTINGS_KEY);
    foreach ($keys as $key) {
      $config = $config->get($key);
    } // foreach
    return $config;
  } // get

  /**
   *
   */
  public static function getValue(string ...$keys) {
    return self::get(...$keys)->getValue();
  } // getValue

  /**
   *
   */
  public static function getDataDefinition(string ...$keys) {
    return self::get(...$keys)->getDataDefinition();
  } // getDataDefinition

  /**
   *
   */
  public static function getDescription(...$keys) {
    return t(self::getDataDefinition(...$keys)->getDescription());
  } // getDescription

  /**
   *
   */
  public static function getLabel(...$keys) {
    return t(self::getDataDefinition(...$keys)->getLabel());
  } // getLabel

  /**
   *
   */
  public static function getSettings(...$keys) {
    return self::getDataDefinition(...$keys)->getSettings();
  } // getSettings

} // BootSettings
